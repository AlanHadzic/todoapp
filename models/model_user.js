const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcryptjs = require('bcryptjs');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not valid email!'
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
    },
    token: [{
        access: {
            type: String,
            required: true,
        },
        token: {
            type: String,
            required: true,
        }
    }]
});

/*
 * INSTANCE methods
 */
UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();

    return _.pick(userObject, ['_id', 'email']);
};

UserSchema.methods.generateAuthToken = function() {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({_id: user._id.toHexString(), access}, 'secret').toString();

    user.token.push({
        access,
        token,
    });

    return user.save().then(() => {
        return token
    });
};

UserSchema.methods.removeToken = function(token) {
    var user = this;

    return user.update({
        $pull: {
            token: {
                token: {token},
            }
        }
    });
};

/*
 * MODEL methods
 */
UserSchema.statics.findByToken = function (token) {
    var User = this;
    var decoded;

    try {
        decoded = jwt.verify(token, 'secret');
    } catch (err) {
        return new Promise((resolve, reject) => {
            reject();
        });
        // return Promise.reject();
    }

    return User.findOne({
        _id: decoded._id,
        'token.token': token,
        'token.access': 'auth',
    });
};

UserSchema.statics.findByCredentials = function (email, password) {
    var User = this;

    return User.findOne({email}).then((user) => {
        if (!user){
            return Promise.reject();
        }
        return new Promise((resolve, reject) => {
            bcryptjs.compare(password, user.password, (err, res) => {
                if (res){
                    resolve(user);
                } else {
                    reject();
                }
            });
        });
    })
};

/*
 * HASHING PASSWORD
 */
UserSchema.pre('save', function (next) {
    var user = this;

    if (user.isModified('password')) {
        bcryptjs.genSalt(10, (err, salt) => {
            bcryptjs.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

var User = mongoose.model('user', UserSchema);

module.exports = { User };