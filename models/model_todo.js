const mongoose = require('mongoose');

var Todo = mongoose.model('todolist', {
    title: {
        type: String,
        minLength: 1,
        maxLength: 50,  
        required: true,
    },
    description: {
        type: String,
        minLength: 1,
        maxLength: 250,
        required: true,
    },
    completedAt: {
        type: Date,
    },
    completed: {
        type: Boolean,
        default: false,
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    }
});

module.exports = { Todo };