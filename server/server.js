
var env = process.env.NODE_ENV || 'development'

if (env === 'development') {
    process.env.PORT = 3000
    process.env.MONGODB_URI = 'mongodb://localhost:27017/TodoApp'
} else if (env === 'test') {
    process.env.PORT = 3000
    process.env.MONGODB_URI = 'mongodb://localhost:27017/TodoAppTEST'
}


const express = require('express');
const bodyParser = require('body-parser');
const _ = require('underscore');

const {mongoose} = require('../db/db_todo');
const {ObjectID} = require('mongodb');
const {Todo} = require('../models/model_todo');
const {User} = require('../models/model_user');

const {authenticate} = require('../middleware/authenticate');

var app = express();
app.use(bodyParser.json());

// TODO NO AUTHENTICATION 
app.post('/todos', (req, res) => {
    var todo = new Todo({
        title: req.body.title,
        description: req.body.description,
    });
  
    todo.save().then((doc) => {
      res.send(doc);
    }, (e) => {
      res.status(400).send(e);
    });
  });

  app.get('/todos', (req, res) => {
    Todo.find({}).then((doc)=>{
        res.send(doc);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.get('/todos/:id', (req, res) => {
    let id = req.params.id;
    if(!ObjectID.isValid(id)){
        return res.status(400).send('Invalid ID');
    }
    Todo.findOne({
        _id: id,
    }).then((doc) => {
        if(doc){
            return res.send(doc);
        }
        res.send(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.send(400).send(err);
    })
});

app.delete('/todos/:id', (req, res) => {
    let id = req.params.id;
    if(!ObjectID.isValid(id)){
        return res.status(400).send('Invalid ID');
    }
    Todo.findByIdAndRemove({
        _id: id,
    }).then((doc) => {
        if(doc){
            return res.send(doc);
        }
        res.status(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.patch('/todos/:id', (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['title', 'description']);

    if(!ObjectID.isValid(id)) {
        return res.status(400).send('Invalid ID');
    }
    Todo.findByIdAndUpdate({ _id: id}, {$set: body}, {new: true}).then((todo) => {
        if(todo) {
            return res.send(todo);
        }
        res.status(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.status(400).send(err);
    });
});

// TODO AUTHENTICATION
app.get('/todo', authenticate, (req, res) => {
    Todo.find({
        _creator: req.user._id,
    }).then((doc)=>{
        res.send(doc);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.get('/todo/:id', authenticate, (req, res) => {
    let id = req.params.id;
    if(!ObjectID.isValid(id)){
        return res.status(400).send('Invalid ID');
    }
    Todo.findOne({
        _id: id,
        _creator: req.user._id,
    }).then((doc) => {
        if(doc){
            return res.send(doc);
        }
        res.send(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.send(400).send(err);
    })
}); 

app.post('/todo', authenticate, (req, res) => {
    var todo = new Todo({
        title: req.body.title,
        description: req.body.description,
        _creator: req.user._id,
    });

    todo.save().then((doc) => {
        res.send(doc);
    }, (err) => {
        res.status(400).send(e);
    });
});

app.delete('/todo/:id', authenticate, (req, res) => {
    let id = req.params.id;
    if(!ObjectID.isValid(id)){
        return res.status(400).send('Invalid ID');
    }
    Todo.findByIdAndRemove({
        _id: id,
        _creator: req.user._id,
    }).then((doc) => {
        if(doc){
            return res.send(doc);
        }
        res.status(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.delete('/executed', authenticate, (req, res) => {
    Todo.remove({completed: true}, (err) => {
        if(err){
            return res.send(err);
        }
        res.send('Successfully removed executed tasks');
    })
});

app.patch('/todo/:id', authenticate, (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['title', 'description']);

    if(!ObjectID.isValid(id)) {
        return res.status(400).send('Invalid ID');
    }
    Todo.findByIdAndUpdate({ _id: id, _creator: req.user._id}, {$set: body}, {new: true}).then((todo) => {
        if(todo) {
            return res.send({todo});
        }
        res.status(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.patch('/todo/executed/:id', authenticate, (req, res) =>{
    let id = req.params.id;
    let body = _.pick(req.body, ['completedAt', 'completed']);
    body.completedAt = new Date().toLocaleString();

    if(!ObjectID.isValid(id)){
        return res.status(400).send('Invalid ID');
    }

    Todo.findByIdAndUpdate({ _id: id, _creator: req.user._id}, {$set: body}, {new: true}).then((todo) => {
        if(todo) {
            return res.send({todo});
        }
        res.status(404).send(`Cannot find document with ID: ${id}`);
    }, (err) => {
        res.status(400).send(err);
    });
});

// USERS
app.post('/user', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch((err) => {
        res.status(400).send(err);
    });
});

app.post('/users/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        return user.generateAuthToken().then((token) => {
            res.header('x-auth', token).send(user);
        });
    }).catch((err) => {
        res.status(400).send();
    });
});

app.delete('/users/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
       res.send(); 
    }, (err) => {
        res.status(400).send();
    })
});

app.get('/users/me', authenticate, (req, res) => {
    res.send(req.user);
});


app.listen(3000, () => {
    console.log('Server is running on http://localhost:3000');
});

module.exports = {
    app
}