const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb')

const {app} = require('../server/server');
const {Todo} = require('../models/model_todo');
const {todos, populateTodos, users, populateUsers} = require('./seed/seed')

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    var text = {"title": "asdasdasdcom", "description":"asd123asd"}

    request(app)
      .post('/todos')
      .send(text)
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(text.title);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Todo.find(text).then((todos) => {
          expect(todos[0].title).toBe(text.title);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should not create todo with invalid body data', (done) => {
    request(app)
      .post('/todos')
      .send({})
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Todo.find().then((todos) => {
          expect(todos.length).toBe(2);
          done();
        }).catch((e) => done(e));
      });
  });
});

describe('GET /todos', () => {
    it('Should get all Todos', (done) => {
        request(app)
            .get('/todos')
            .expect(200)
            .expect((res) => {
                expect(res.body.length).toBe(2)
            })
            .end(done)
    })

    it('Should get Todos with id', (done) => {
        request(app)
            .get(`/todos/${todos[0]._id.toHexString()}`)
            .expect(200)
            .expect((res) => {
                expect(res.body.title).toBe(todos[0].title)
            })
            .end(done)
    })

    it('Should return 404 if todo not found', (done) => {
        request(app)
            .get(`/todosBLA`)
            .expect(404)
            .end(done)
    })
})

describe('DELETE /todos', () => {
    it('Sholud delete todo', (done) => {
        request(app)
            .delete(`/todos/${todos[0]._id.toHexString()}`)
            .expect(200)
            .end(done)
    })
})

describe('PATCH /todos', () => {
    it('Should update the todo', (done) => {
        request(app)
            .patch(`/todos/${todos[0]._id.toHexString()}`)
            .expect(200)
            .send({"title":"blabla", "description": "lololo"})
            .expect((res) => {
                expect(res.body.title).toBe("blabla")
            })
            .end(done)
    })
})

describe('GET users/me', () => {
    it('Sould return user if authenticated', (done) => {
        request(app)
            .get('/users/me')
            .set('x-auth', users[0].token[0].token)
            .expect(200)
            .expect((res) => {
                expect((res.body._id).toBe(users[0]._id.toHexString()))
            })
            .end(done)
    })
})